import {test, expect} from '@playwright/test'
// TODO: Apply this config
test.beforeEach(async ({ page, baseURL, context, browser }) => {
    context = await browser.newContext();
    page = await context.newPage();
    await page.goto(baseURL);
});

test.afterAll(async ({page, context}) => {
    await context.close();
    await page.close();
});