import { test, expect } from '@playwright/test';
import { HomePage } from '../pages/home-page';
import { ProductListPage } from '../pages/product-list-page';
import { ProductDetailPage } from '../pages/product-detail-page';

test.beforeEach(async ({ page, baseURL }) => {
  await page.goto(baseURL);
});

test('PLP Navigation',async ({page}) => {
  const homePage = new HomePage(page);
  await homePage.waitForPageLoaded();
  await homePage.gotoCameraCategory();

  const plp = new ProductListPage(page);
  await plp.waitForPageLoaded();
  await plp.filterByMaxPrice('1000');
  await plp.filterByColor();

})


test('Add to cart',async ({page}) => {
  const homePage = new HomePage(page);
  await homePage.waitForPageLoaded();
  await homePage.gotoCameraCategory();

  const plp = new ProductListPage(page);
  await plp.waitForPageLoaded();
  await plp.selectProduct();

  const pdp = new ProductDetailPage(page);
  await pdp.waitForPageLoaded();
  await pdp.addProductToCart();
  await pdp.gotoCheckout();
  
})