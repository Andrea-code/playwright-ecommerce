import { test, expect } from '@playwright/test';
import { RegistrationPage } from '../pages/registration-page';
import { HomePage } from '../pages/home-page';
import { assert } from 'console';

test.beforeEach(async ({ page, baseURL }) => {
    await page.goto(baseURL);
  });

test('User registration', async ({page})=>{
    const home = new HomePage(page);
    await home.waitForPageLoaded();
    await home.gotoAccountLoginPage();

    const register = new RegistrationPage(page);
    await register.waitForPageLoaded();
    await register.gotoRegisterForm();
    await register.fillForm();
    await register.submitForm();
    const currentURL = await page.url();
    await expect(currentURL).toContain('account/success');
});

