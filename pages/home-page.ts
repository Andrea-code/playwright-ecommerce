import {expect, type Page, type Locator} from '@playwright/test';

export class HomePage{

    readonly page: Page;
    readonly categoriesLink: Locator;
    readonly myAccountLink: Locator;
    readonly categoriesSidebar: Locator;
    readonly cameraCategoryLink: Locator;

    constructor(page: Page){
        this.page = page;
        this.categoriesLink = page.getByRole('button', { name: 'Shop by Category' });
        this.myAccountLink = page.getByRole('button', { name: ' My account' });
        this.categoriesSidebar = page.locator('#mz-component-1626147655');
        this.cameraCategoryLink = page.getByRole('link', { name: 'Cameras', exact: true });
    }

    async waitForPageLoaded(){
        await this.page.waitForTimeout(1000);
    }

    async gotoCameraCategory(){
        await this.categoriesLink.click();
        await expect(this.categoriesSidebar).toBeVisible();
        await this.cameraCategoryLink.click();
    }

    async gotoAccountLoginPage(){
        await this.myAccountLink.click();
    }

}