import { expect, Page, Locator } from "@playwright/test";

export class RegistrationPage {
    readonly page : Page;
    readonly newCustomerBtn : Locator;
    readonly nameInput : Locator;
    readonly lastNameInput : Locator;
    readonly emailInput : Locator;
    readonly phoneInput : Locator;
    readonly passwordInput : Locator;
    readonly passwordConfirmInput : Locator;
    readonly termsCheckbox : Locator;
    readonly submitButton : Locator;

    constructor(page : Page){
        this.page = page;
        this.newCustomerBtn = page.getByRole('link', { name: 'Continue' });
        this.nameInput =  page.getByPlaceholder('First Name');
        this.lastNameInput = page.getByPlaceholder('Last Name');
        this.emailInput =  page.getByPlaceholder('E-Mail');
        this.phoneInput =  page.getByPlaceholder('Telephone');
        this.passwordInput =  page.getByPlaceholder('Password', { exact: true });
        this.passwordConfirmInput =  page.getByPlaceholder('Password Confirm');
        this.termsCheckbox = page.getByText('I have read and agree to the');
        this.submitButton = page.getByRole('button', { name: 'Continue' });
    }

    async waitForPageLoaded(){
        await this.page.waitForTimeout(1000);
    }

    async gotoRegisterForm(){
        await this.newCustomerBtn.click();
    }

    async fillForm(){
        await this.nameInput.fill('NameTest');
        await this.lastNameInput.fill('LastName Test');
        console.log(this.generateTestEmail());
        await this.emailInput.fill(await this.generateTestEmail());
        await this.phoneInput.fill('5555555555');
        await this.passwordInput.fill('Passw0rd.');
        await this.passwordConfirmInput.fill('Passw0rd.');
        await this.termsCheckbox.click();

        await this.page.waitForTimeout(1000);
    }

    async submitForm(){
        await this.submitButton.click();
        await this.page.waitForTimeout(1000);
    }

    async generateTestEmail(): Promise<string>{
        const currentDate = Date.now();
        return `email_${currentDate}@test.com`;
    }
}