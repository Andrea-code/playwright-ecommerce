import { expect, type Page, type Locator } from "@playwright/test";

export class ProductListPage {

    readonly page: Page;
    readonly filterSelect : Locator;
    readonly priceInputFilter : Locator;
    readonly colorFilter : Locator;
    readonly product : Locator;

    constructor(page: Page) {
        this.page = page;
        this.product = page.getByRole('link', { name: 'HTC Touch HD HTC Touch HD HTC' });
        this.filterSelect = page.locator('#input-sort-212403');
        this.priceInputFilter = page.locator('#mz-filter-panel-0-0').getByPlaceholder('Maximum Price');
        this.colorFilter = page.locator('#mz-filter-panel-0-4').getByRole('img', { name: 'Pink' });  
    }

    async waitForPageLoaded(){
        await this.page.waitForTimeout(1000);
    }

    async scrollDown(){
        await this.page.evaluate(() =>{
            window.scrollBy(0,500);
        });
    }

    async moveTo(element : Locator){
        await element.scrollIntoViewIfNeeded();
    }

    async filterByMaxPrice(price : string){
        await this.moveTo(this.filterSelect);
        await this.filterSelect.scrollIntoViewIfNeeded();
        await this.priceInputFilter.click();
        await this.priceInputFilter.fill(price);
        await this.priceInputFilter.press('Enter');

        await this.page.waitForTimeout(1500);
    }

    async filterByColor(){
        await this.colorFilter.click();
        await this.page.waitForTimeout(1500);
    }

    async selectProduct(){
        await this.product.click();
    }
}