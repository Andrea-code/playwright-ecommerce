import {expect, type Page, type Locator} from '@playwright/test';

export class ProductDetailPage {

    readonly page: Page;
    readonly addCartBtn: Locator;
    readonly inStockLabel: Locator;
    
    constructor(page:Page){
        this.page = page;
        this.addCartBtn = page.getByRole('button', {name: 'Add to cart'});
        this.inStockLabel = page.getByText('In Stock');
    }

    async waitForPageLoaded(){
        await this.page.waitForTimeout(1000);
    }

    async addProductToCart(){
        try {
            await this.verifyStock();
            await this.addCartBtn.click();
            await this.page.waitForTimeout(800);
        } catch (error) {
            console.error('Error for add to car item', error);
        }
    }

    async verifyStock(){
        if(!this.inStockLabel){
            throw new Error('There is not stock for the current product');
        }
    }

    async gotoCheckout(){
        await this.page.getByRole('link', { name: 'Checkout ' }).click();
    }

}